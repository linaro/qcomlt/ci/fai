#!/bin/sh

set -ex

# Get our version of udevadm to be executed
export PATH=`pwd`/tools:$PATH

#sudo apt update
#sudo apt -y install cargo rustc

# Debian bookworm has too old cargo. Use rustup instead
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y

export PATH=~/.cargo/bin:$PATH
cargo install android-sparse
which img2simg

if [ "$FAI_KIND" = "EFI" ]
then
    CLASS_EXTRA=EFI,${CLASS_EXTRA}
fi

CLASS_EXTRA=${CLASS_EXTRA%,}

HOSTNAME=linaro-${ROOTFS}
CLASS=$(echo SAVECACHE,${OS_FLAVOUR},DEBIAN,LINARO,QCOM,${ROOTFS},RAW,${CLASS_EXTRA} | tr '[:lower:]' '[:upper:]')
[ -n "$CLASS_EXTRA" ] && ROOTFS="$ROOTFS-$(echo "$CLASS_EXTRA" | tr -s ',' '-')"
OUT=${VENDOR}-${OS_FLAVOUR}-${ROOTFS}-${PLATFORM_NAME}-${CI_PIPELINE_ID}

fai-diskimage -v --cspace $(pwd) --hostname ${HOSTNAME} -S ${ROOTFS_SZ} --class ${CLASS} ${OUT}.img.raw

sudo cp /var/log/fai/${HOSTNAME}/last/fai.log out/fai-${ROOTFS}.log

if grep -E '^(ERROR:|WARNING: These unknown packages are removed from the installation list|Exit code task_)' out/fai-${ROOTFS}.log
then
    echo "Errors during build"
    rm -rf out/*
    exit 1
fi

rootfs_sz_real=$(du -h ${OUT}.img.raw | cut -f1)

if [ "$FAI_KIND" = "EFI" ]
then
    loop=$(losetup -P --show -f ${OUT}.img.raw)

    img2simg ${loop}p1 out/${OUT}-esp.img
    pigz -9 out/${OUT}-esp.img

    img2simg ${loop}p2 out/${OUT}.img
    pigz -9 out/${OUT}.img

    losetup -d ${loop}

    mv ${OUT}.img.raw out/${OUT}-usb.img
    pigz -9 out/${OUT}-usb.img
else
    img2simg ${OUT}.img.raw out/${OUT}.img
    rm ${OUT}.img.raw
    pigz -9 out/${OUT}.img
fi

# dpkg -l output
mv out/packages.txt out/${OUT}.packages

# copy out dir into $FAI_CACHE/$ROOTFS-$ROOTFS_SZ/out
# so a later stage can check md5sums
mkdir -p    $FAI_CACHE/$ROOTFS-$ROOTFS_SZ
mv out/* $FAI_CACHE/$ROOTFS-$ROOTFS_SZ
