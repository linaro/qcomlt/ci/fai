set -ex

# Create boot image for each DTB
BOOTIMG_PAGESIZE='4096'
BOOTIMG_BASE='0x80000000'
BOOTIMG_KERNEL_OFFSET='0x8000'
BOOTIMG_RAMDISK_OFFSET='0x1000000'
BOOTIMG_TAGS_OFFSET='0x100'

dtb_name=$(basename $(echo ${DTB} | tr '/' '_' ) .dtb)

# create output directory
BOOTIMAGE_OUT_DIR="$BOOTIMAGE_CACHE/$dtb_name"
mkdir -p $BOOTIMAGE_OUT_DIR

KERNEL_CMDLINE="root=${ROOTFS} console=tty0 console=${SERIAL_CONSOLE},115200n8 ${KERNEL_CMDLINE_PLATFORM}"

BOOTIMG=boot-${VENDOR}-${OS_FLAVOUR}-${PLATFORM_NAME}-${dtb_name}-${CI_PIPELINE_ID}.img


cp $KERNEL_CACHE/dtbs/${DTB} $BOOTIMAGE_OUT_DIR/${dtb_name}.dtb
cat $KERNEL_CACHE/vmlinuz-* $BOOTIMAGE_OUT_DIR/${dtb_name}.dtb > Image.gz+dtb

mkbootimg \
    --kernel Image.gz+dtb \
    --ramdisk $KERNEL_CACHE/initrd.img-* \
    --output $BOOTIMAGE_OUT_DIR/${BOOTIMG} \
    --pagesize "${BOOTIMG_PAGESIZE}" \
    --base "${BOOTIMG_BASE}" \
    --kernel_offset "${BOOTIMG_KERNEL_OFFSET}" \
    --ramdisk_offset "${BOOTIMG_RAMDISK_OFFSET}" \
    --tags_offset "${BOOTIMG_TAGS_OFFSET}" \
    --cmdline "${KERNEL_CMDLINE}"

pigz -9 $BOOTIMAGE_OUT_DIR/${BOOTIMG}
