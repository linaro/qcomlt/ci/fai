#!/bin/sh

set -ex

mkdir -p out

# no need to upload dtbs related files
rm -rf ${KERNEL_CACHE}/dtbs

# copy both caches (fai and bootimage) into 'out' folder
mv ${FAI_CACHE}/*/* ${KERNEL_CACHE}/* ${BOOTIMAGE_CACHE}/*/* $CI_PROJECT_DIR/out/

# Record info about kernel, there can be multiple .packages files, but we have already checked that kernel version is the same. so pick one.
kernel_binpkg=$(grep -h linux-image out/*.packages | sed 's/\s\s*/ /g' | cut -d ' ' -f2 | uniq)
kernel_pkgver=$(grep -h linux-image out/*.packages | sed 's/\s\s*/ /g' | cut -d ' ' -f3 | uniq)

# Build information
cat > out/HEADER.textile << EOF

h4. QCOM Landing Team -  #${CI_PIPELINE_ID}

Build description:
* Build URL: "$CI_PIPELINE_URL":$CI_PIPELINE_URL
* OS flavour: $OS_FLAVOUR
* FAI: "$CI_PROJECT_URL":$CI_PROJECT_URL
* FAI commit: "$CI_COMMIT_SHA":$CI_PROJECT_URL/-/commit/$CI_COMMIT_SHA
* Kernel package name: ${kernel_binpkg}
* Kernel package version: ${kernel_pkgver}
EOF


cat > out/README.textile << EOF
h4. Build changes

EOF

# Record build log changes in git tree
LT_CI_REPO_URL=$(echo ${CI_REPOSITORY_URL} | sed -e "s!.*@!https://oauth2:${REPO_ACCESS_TOKEN}@!g")
if [ -d lt-ci ]; then
    (cd lt-ci; git pull --prune --force ${LT_CI_REPO_URL} debian/${PLATFORM_NAME})
elif git ls-remote ${LT_CI_REPO_URL} | grep debian/${PLATFORM_NAME} ; then
    git clone ${LT_CI_REPO_URL} -b debian/${PLATFORM_NAME} lt-ci
else
    mkdir lt-ci
    cd lt-ci
    git init -b debian/${PLATFORM_NAME}
    git commit --allow-empty -m 'Initial commit'
    git remote add origin ${LT_CI_REPO_URL}
    cd -
fi

for pkg in out/*packages ; do
    ROOTFS="$(basename ${pkg} -${PLATFORM_NAME}-${CI_PIPELINE_ID}.packages)"
    LT_CI_PACKAGES="lt-ci/${ROOTFS}.packages"
    if [ -r "${LT_CI_PACKAGES}" ] ; then
        echo "h5. Packages changes for ${ROOTFS}" >> out/README.textile
        echo >> out/README.textile
        echo "pre.. " >> out/README.textile
        python3 ci/debpkgdiff.py "${LT_CI_PACKAGES}" ${pkg} >> out/README.textile
        echo >> out/README.textile
    else
        echo "no packages list for ${ROOTFS}, skipping diff report"
    fi

    cp ${pkg} ${LT_CI_PACKAGES}
done

# record kernel config changes since last build, if available
if [ -r lt-ci/config ] ; then
    echo "h5. Changes for kernel config" >> out/README.textile
    echo >> out/README.textile
    echo "pre.. " >> out/README.textile
    diff -su lt-ci/config out/config-* >> out/README.textile || true
    echo >> out/README.textile
else
    echo "latest build published does not have kernel config, skipping diff report"
fi

# record kernel config changes in git
cp out/config-* lt-ci/config

# Commit build changes in lt-ci
cd lt-ci
git add -A
git commit --allow-empty -m "Import build ${CI_PIPELINE_ID}"
git push origin debian/${PLATFORM_NAME}
cd -

# create a sums for all artifacts to be uploaded
(cd $CI_PROJECT_DIR/out && md5sum * | tee MD5SUMS.txt)

# publish artifacts
time python3 $(which linaro-cp.py) --link-latest $CI_PROJECT_DIR/out ${PUB_DEST_PREFIX}member-builds/qcomlt/debian/${PLATFORM_NAME}-${OS_FLAVOUR}/${CI_PIPELINE_ID}
